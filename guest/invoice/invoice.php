<?php
/* Report all errors except E_NOTICE */
error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
$OO=$_GET["OO"];
$II=$_GET["II"];

$OOS=$IIS="";
$n="0";
if($OO==""&&$II==""){
exit('<div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i> Error!</h4>
                The data of new product already exists!.
              ');
}
else  $sql="SELECT * FROM invoice where ";
if($OO<>"")
{
	$OOS="OrderID = '".$OO."' ";
	$sql.=$OOS;
  $n++;
}
if($II!="")
{
  if($n<>"0") $sql.="and ";
	$IIS="invoiceID = '".$II."' ";
	$sql.=$IIS;
}

header("content-type: text/html;charset=utf-8");
$DB_HOST = "127.0.0.1";
$DB_UESR = "root";
$DB_PASSWORD ="password";
$DB_NAME ="bdcrm";

//connect mysql
$link = new MySQLi($DB_HOST,$DB_UESR,$DB_PASSWORD,$DB_NAME);
mysqli_select_db($link,"bdcrm");


$result = mysqli_query($link,$sql);
$row = mysqli_fetch_assoc($result);

$sql1="SELECT * FROM Orders where ";
$OOS="OrderID = '".$row['OrderID']."' ";
$sql1.=$OOS;
$result1 = mysqli_query($link,$sql1);
$row1 = mysqli_fetch_assoc($result1);

$sql2="SELECT * FROM Shippers where ";
$OOS="OrderID = '".$row['OrderID']."' ";
$sql2.=$OOS;
$result2 = mysqli_query($link,$sql2);
$row2 = mysqli_fetch_assoc($result2);

$sql3="SELECT * FROM OrderDetails where ";
$OOS="OrderID = '".$row['OrderID']."' ";
$sql3.=$OOS;
$result3 = mysqli_query($link,$sql3);
$row3 = mysqli_fetch_assoc($result3);

$sql4="SELECT * FROM Products where ";
$OOS="ProductID = '".$row3['ProductID']."' ";
$sql4.=$OOS;
$result4 = mysqli_query($link,$sql4);
$row4 = mysqli_fetch_assoc($result4);

$sql5="SELECT * FROM Customer where ";
$OOS="custID = '".$row1['custID']."' ";
$sql5.=$OOS;
$result5 = mysqli_query($link,$sql5);
$row5 = mysqli_fetch_assoc($result5);

$tem=$row[amount]*$row3[Unitprice]*0.093;
$mony=$tem+$row[amount]*$row3[Unitprice]+5.80;
echo <<<END_OF_TAT

<section>
	<div class="row">
		<div class="col-xs-12 table-responsive">
			<table class="table table-striped">
				<thead>
				<tr>
					<td>Qty</td>
					<td>Product</td>
					<td>Serial # </td>
					<td>Description</td>
					<td>Subtotal</td>
				</tr>
				</thead>
				<tbody>
				<tr>
					<td>$row[amount]</td>
					<td>$row4[ProductName]</td>
					<td>$row4[ProductID]</td>
					<td>$row4[QuantityPerUnit]</td>
					<td>$row4[Unitprice]</td>
				</tr>

				</tbody>
			</table>
		</div>

	</div>
</section>
END_OF_TAT;

echo <<<END_OF_TEXT

<section class="invoice">

	<div class="row">
		<div class="col-xs-12">
			<h2 class="page-header"> <em class="fa fa-globe"></em> BigDock, Inc. <small class="pull-right">$row[writeDate]</small> </h2>
		</div>

	</div>
	<div class="row invoice-info">
		<div class="col-sm-4 invoice-col">
			From
			<address>
				<strong>  $row2[ShipperName], Inc.</strong><br>
				$row1[ShipAddress]<br>
				<strong>Phone:</strong> $row2[ShipperTel]
			</address>
		</div>

		<div class="col-sm-4 invoice-col">
			To
			<address>
				<strong>  $row5[custName]</strong><br>
				$row5[custCity]<br>
				<strong>Phone:</strong> $row5[custTel]<br>
			</address>
		</div>

		<div class="col-sm-4 invoice-col">
			<b>Invoice #007612</b><br>
			<br>
			<b>Order ID:</b> $row[OrderID]<br>
			<b>Payment Due:</b> $row[writeDate]<br>
			<b>Account:</b> $row1[custID]
		</div>

	</div>



	<div class="row">

		<div class="col-xs-6">
			<p class="lead">Payment Methods:</p>
			<img src="../../dist/img/credit/visa.png" alt="Visa">
			<img src="../../dist/img/credit/mastercard.png" alt="Mastercard">
			<img src="../../dist/img/credit/american-express.png" alt="American Express">
			<img src="../../dist/img/credit/paypal2.png" alt="Paypal">

			<p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
				Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya handango imeem plugg
				dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra.
			</p>
		</div>

		<div class="col-xs-6">
			<p class="lead">Amount Due 2/22/2014</p>

			<div class="table-responsive">
				<table class="table">
					<tbody><tr>
						<th style="width:50%">Subtotal:</th>
						<td>$row4[Unitprice]</td>
					</tr>
					<tr>
						<th>Tax (9.3%)</th>
						<td>$tem</td>
					</tr>
					<tr>
						<th>Shipping:</th>
						<td>$5.80</td>
					</tr>
					<tr>
						<th>Total:</th>
						<td>$mony</td>
					</tr>
				</tbody></table>
			</div>
		</div>

	</div>



	<div class="row no-print">
		<div class="col-xs-12">
			<a href="#####" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a>

		</div>
	</div>
</section>

END_OF_TEXT;

mysqli_close($link);

?>
