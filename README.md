# README #
根目录下，login.html是登陆页面，其中对应/php/login.php，当前此php文件能查询到登陆用户的type，但没有相应的ajax用来传值到登陆后进入页面跟目下的index.html

#已实现功能#
1. 表单的查询，多条件限制查询
## 实现方法##
相应的<input><select>用来输入条件，通过最后<button>中调用ShowTable()函数来将对应php查询到的值显示到html下文
中的<table>中
### 例如 ###
/table/order.html&&/table/orderselect.php&&../dist/js/orderselect.js三者为一套orders表查询功能，首先，在html中的查询条件确立之后，通过调用orderselect.js中的Show Table（）来进行表的显示，js中，用document.getElementById("ID").value函数获取html中相对应ID的值，存入js变量中，
url=url+"?O="+O+"&C="+C+"&E="+E
url=url+"&sid="+Math.random()
xmlHttp.onreadystatechange=stateChanged 
xmlHttp.open("GET",url,true)
xmlHttp.send(null)
将多变量传入orderselect.php进行查询，有get转译，防止sql注入
最后php中查询的表通过AJAX的方式（orderselect.js）返回html显示

2. 注册。guest用户注册（emp的注册由boss也就是超级管理员在后台进行）
## 实现方法 ##
通过register.html&&regist.php的运行，执行注册功能
### 未完成地方，注册完了就完了，没任何客户端反应，但功能是有的 ###